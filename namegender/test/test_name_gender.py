#!/usr/bin/python3
# -*- coding: utf-8 -*-

#  Copyright (C) 2021 David Arroyo Menéndez

#  Author: David Arroyo Menéndez <davidam@gmail.com>
#  Maintainer: David Arroyo Menéndez <davidam@gmail.com>
#  This file is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This file is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with namegender; see the file LICENSE.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301 USA,

import unittest
import numpy as np
import os
from namegender.src.core import Gender

class TddInPythonExample(unittest.TestCase):

    def test_dame_gender_name_frec(self):
        g = Gender()
        frec1 = g.name_frec("INES")
        self.assertEqual(int(frec1['females']), 149158)
        self.assertEqual(int(frec1['males']), 515)
        frec2 = g.name_frec("BEATRIZ")
        self.assertEqual(int(frec2['females']), 149999)
        frec3 = g.name_frec("ALMUDENA")
        self.assertEqual(int(frec3['females']), 30496)
