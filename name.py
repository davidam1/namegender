#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Copyright (C) 2021 David Arroyo Menéndez

#  Author: David Arroyo Menéndez <davidam@gmail.com>
#  Maintainer: David Arroyo Menéndez <davidam@gmail.com>
#  This file is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This file is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with namegender; see the file LICENSE.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301 USA,


from namegender.src.core import Gender
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("name", help="display the gender")
args = parser.parse_args()

g = Gender()

num_males = int(g.name_frec(args.name)['males'])
num_females = int(g.name_frec(args.name)['females'])
print("%s males for %s from namegender statistics" % (num_males, args.name))
print("%s females for %s from namegender statistics" % (num_females, args.name))


if (num_females > num_males):
    per = (num_females / (num_females + num_males)) * 100
    print("There are %s percentage of females" % per)
elif (num_males >= num_females):
    per = (num_males / (num_females + num_males)) * 100
    print("There are %s percentage of males" % per)
